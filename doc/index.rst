.. image:: ../images/logo-uga.png
   :width: 30pt

.. image:: ../images/logo_cnrs_small.png
   :width: 30pt

Welcome to rringg's documentation!
==================================

.. toctree::
    :maxdepth: 2
    :caption: Documentation

    installation
    test
    contacts
    authors
    funding
    changes
    modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

