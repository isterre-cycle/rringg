Supported data formats
======================

As of InsarViz 1.0.0, supported input data format include all `GDAL <https://gdal.org/index.html>`_ types such as:

* NSBAS-type time series (depl_cumule and associated files)
* GeoTiffs
* VRT (Virtual Datasets): this GDAL format is a vritual dataset composed of other GDAL datasets with repositioning, and algorithms potentially applied as well as various kinds of metadata altered or added (see `documentation <https://gdal.org/drivers/raster/vrt.html>`_). For instance, you can make a VRT composed of individual displacement map files or interferograms. We provide a `script here <https://gricad-gitlab.univ-grenoble-alpes.fr/deformvis/insarviz/-/snippets/266>`_ to create a VRT.

We are developing on a mechanism to allow users to provide a **plug-in-type loader to load other types of data**. Stay tuned!
