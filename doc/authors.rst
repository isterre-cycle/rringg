Contributors
============

Here are a list of people that work, or have worked, on RRIINGG

* UGA stands for University Grenoble Alpes (https://www.univ-grenoble-alpes.fr/).
* ISTerre stands for Institut des Sciences de la Terre (https://www.isterre.fr).

Current contributors
--------------------

Alphabetic order of last name : 

* Aubin Bleriot Tsapong Tsague (ISTERRE/UGA),  aubin-bleriot.tsapong-tsague@univ-grenoble-alpes.fr
* Marie-Pierre Doin (ISTERRE/CNRS) marie-pierre.doin@univ-grenoble-alpes.fr
* Gaël Janex (ISTERRE/CNRS) gael.janex@univ-grenoble-alpes.fr
* Erwan Pathier (ISTERRE/UGA) erwan.pathier@univ-grenoble-alpes.fr
* Anne Socquet (ISTERRE/UGA) anne.socquet@univ-grenoble-alpes.fr
* Franck Thollard (ISTERRE/CNRS) franck.thollard@univ-grenoble-alpes.fr

Former contributors
-------------------

Nop

Contact
-------

Anne Socquet (ISTERRE/UGA) anne.socquet@univ-grenoble-alpes.fr

