# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
from shutil import copyfile
import os
import sys

sys.path.insert(0, os.path.abspath('..'))
print(f"PATH PATH: {os.path.abspath('.')}")
import rringg

# copy Author file to the right place
mydir = os.path.dirname(os.path.realpath(__file__))
copyfile(mydir + "/../AUTHORS.rst", "authors.rst")

# -- Project information -----------------------------------------------------
mydir = os.path.dirname(os.path.realpath(__file__))
copyfile(mydir + "/../AUTHORS.rst", "authors.rst")

# -- Project information -----------------------------------------------------
project = 'rringg'
# Author list
author = "ISTERRE / CNRS / UGA"
copyright = '2024 ' + author

release = rringg.__version__
# release = "0.0.21"
# The short X.Y version (keep semantic versioning)
version = release

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = []

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

root_doc = 'index'


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']
