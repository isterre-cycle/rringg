############
Installation
############


Recommended Python distribution
-------------------------------

In order to run RRINGG code you will need a python distribution. We recommend the `Anaconda distribution <https://www.anaconda.com/products/individual>`_ (version 3.6 or higher).


Code download
-------------

First, download the source code, typically using git:

.. code-block :: bash

    git clone git@gricad-gitlab.univ-grenoble-alpes.fr:isterre-cycle/rringg.git 

Environment setup
-----------------

* **With Anaconda**

We recommend you install the RRINGG tool in a virtual environment. If you have installed the `Anaconda distribution <https://www.anaconda.com/products/individual>`_, navigate to within the top-level rringg folder and create a conda environment with the required dependencies using :

.. code-block :: bash

 conda env create -f environment.yaml


And activate it:

.. code-block :: bash

 conda activate rringg-env

* **Without Anaconda**

Without Anaconda, create an virtual environment, activate it and install the required packages using the following commands:

.. code-block :: bash

 python3 -m venv venv
 source venv/bin/activate
 pip install -r requirements.txt



Installation type
------------------
Finally, install the rringg module within your virtual environment. If you do **not** want to modify the source code, follow the **Regular installation** instructions. If you would like to be able to **modify the code**, follow the **Developper install instructions**.

* **Regular installation**

Installing rringg in a virtual environment, or system-wide, is just a one-line command:

.. code-block :: bash

        pip install .

* **Developper installation** 

If you intend to change the source code, you should install the tool in a *editable* mode:

.. code-block :: bash

        pip install -e . 

Test your installation
----------------------

You can check your installation by doing:

.. code-block :: bash

       CODE TO RUN

This should print the help message. If not, your install failed.

