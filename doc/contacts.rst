Contacts & bug reports
======================

In case of problem or if you have any requests, you can contact the 
the developers using the following mailing list : rringg-sos@univ-grenoble-alpes.fr

If you want to stay tuned you can register to the rringg-info mailing list 
by sending an email to rringg-notification@univ-grenoble-alpes.fr with the subject 

"subscribe rringg-notification@univ-grenoble-alpes.fr FIRST_NAME LAST_NAME" 

The traffic of this mailing list is very low (few emails per year) but let you know when new
releases are made. 
