Running all the tests
---------------------

Testing can be done using the *de facto* standard pytest


Navigate to the main rringg folder, and run the test with the following command: 

.. code-block :: bash
    
        pytest



