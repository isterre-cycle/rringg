#!/usr/bin/env python3

"""
caching strategy with at least two possible caches:
- file cache
- irods cache
"""

import os
import abc
import shutil
import json
import logging

# create an abstract class that define which method should be defined

class AbstractCache(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def get(self, value, target):
        """
        Tries to get the value from cache. Return None if not present
        :param value: the value to get from cache
        :type value: str
        :param target: where to put the data recovered
        :type target: defined in inherited class
        :return: the value we aim to get or None if not define
        :rtype: defined in the inherited class
        """
        raise NotImplementedError('users must define "get"" to use this base class')

    @abc.abstractmethod
    def create_cache(self):
        """ create an empty cache  """
        raise NotImplementedError('users must define "create_cache"" to use this base class')


    @abc.abstractmethod
    def set(self, value):
        """
        Put value in cache. Return None if not possible
        :param value: the value to put in cache
        :type value: str
        :return: True if it was ok, False else
        :rtype: Boolean
        """
        raise NotImplementedError('users must define "set" to use this base class')

class DiskFileCache(AbstractCache):
    """ define a file cache on disk. Files are cached on disk """

    def __init__(self, directory, cache_limit=int(4e9), logger="__main__", config_file_name=".cache_config.json"):
        """
        The directory where to put the cached file
        :param directory: a string (that represent a directory)
        :type directory: str
        """
        self._logger_name = logger
        self._logger = logging.getLogger(self._logger_name + "." + __name__ + 'DiskFileCache.__init__')
        # basic checking
        self._config_file_name = config_file_name
        self._directory = directory
        if not os.path.exists(directory):
            self._logger.info("cache directory ({}) does not exist, creating".format(directory))
            self.create_cache(directory, cache_limit)
        else:
            if not os.path.isdir(directory):
                self._logger.error("parameter {} should be a directory".format(directory))
                raise IOError("parameter {} should be a directory".format(directory))
            if  config_file_name not in os.listdir(directory):
                err_str = "cannot consider directory {} as cache dir as it does not contains config file".format(directory)
                self._logger.error(err_str)
                raise IOError("cannot consider directory {} as cache dir as it does not contains config file".format(directory))
        # cache just created or contains config file: loading config.
        self.read_config()
        self._logger.info("config_dict={}".format(self._config_dict))
        self._logger.info('disk cache, caching files: direcory={}, cache_limit={}'.format(directory,
                                                                                        self.get_limit()))


    def get_limit(self):
        """return the size limit of the cache

        :return: the size limit of the cache
        :rtype: int
        """
        return self._config_dict["cache_limit"]

    def get_size(self):
        return self._config_dict["cache_size"]

    def create_cache(self, directory, cache_limit):
        """
         creates an empty cache directory with a given max size
        :param directory: where to put the cache (directory should not exists)
        :type directory: str
        :param cache_limit: maximum cache size (in bytes)
        :type cache_limit: int
        """
        os.makedirs(directory)
        # creating the config file.
        self._config_dict = {"cache_limit": cache_limit, "cache_size":0}
        self.write_config()

    def write_config(self):
        """
        Write the config file with the current parameters.
        """
        with open("{}/{}".format(self._directory, self._config_file_name), "w") as config_handle:
            config_handle.write(json.dumps(self._config_dict))

    def read_config(self):
        """
        read the config file and return a dictionnary of the contains
        :return: the parameters
        :rtype: a dictionnary of the config
        """
        with open("{}/{}".format(self._directory, self._config_file_name), "r") as config_handle:
            data = config_handle.read()
        self._config_dict = json.loads(data)

    def get(self, value, target):
        """
        If a file named value is in directory, copy it to target, else return None.
        Raises ValueError if value contains '/'.
        See shutil.copyfile for exception that can be raised when copying.
        If target is a directory, copy value in target/value, else copy value to target.

        :param value: the file to look at (should be a basename)
        :type value: str
        :param target: a target where to put the value
        :type target str:
        :return: True if the file was available, False if not.
        :rtype: boolean
        """
        self._logger = logging.getLogger(self._logger_name + "." + __name__ + 'DiskFileCache.get')
        self._logger.debug("trying to get {}".format(value))
        if '/' in value:
            raise ValueError("value parameter should be a basename but is {}".format(value))
        cache_path = "{}/{}".format(self._directory, value)
        if os.path.exists(cache_path):
            if os.path.isdir(target):
                target = "{}/{}".format(target, value)
            shutil.copyfile(cache_path, target)
            self._logger.debug("{} in cache, copied to {}".format(value, target))
            return True
        # not in cache
        self._logger.debug("{} not in cache".format(value))
        return False

    def set(self, value):
        """ put value in cache if possible.
        """
        self._logger = logging.getLogger(self._logger_name + "." + __name__ + 'DiskFileCache.set')
        cache_path = "{}/{}".format(self._directory, os.path.basename(value))
        if os.path.exists(cache_path):
            self._logger.info("set(): {} already in cache".format(value))
            return True
        value_size = os.path.getsize(value)
        if self._config_dict["cache_limit"] < value_size:
            self._logger.warn("set(): {} is larger than originial cache size, not caching".format(value))
            return False
        available = self._config_dict["cache_limit"] - self._config_dict["cache_size"]
        if available > value_size:
            shutil.copyfile(value, cache_path)
            self._config_dict["cache_size"] += value_size
            self.write_config()
            self._logger.info("{} add to cache. New cache size is {}".format(value, self._config_dict["cache_size"]))
            return True
        else:
            # we need to remove the oldest files. List files sorted according to their access date
            # and get their name and size
            self._logger.info("freing place in cache for {}".format(value))
            abs_file_list = (self._directory + "/" + f for f in os.listdir(self._directory) if self._config_file_name not in f)
            file_list = sorted([(f, os.stat(f).st_atime, os.stat(f).st_size) for f in abs_file_list],  key=lambda x:x[1])
            removed_size = 0
            to_free = value_size - available
            i=0
            while removed_size < to_free and i < len(file_list):
                removed_size += file_list[i][2]
                os.unlink(file_list[i][0])
                self._config_dict["cache_size"] -= file_list[i][2]
                self._logger.debug("removing {}, which save {} bytes".format(file_list[i][0], file_list[i][2]))
                i += 1
            # we have enough place now
            shutil.copyfile(value, cache_path)
            self._config_dict["cache_size"] += value_size
            self.write_config()
            self._logger.info("{} added to cache. New cache size is {}".format(value, self._config_dict["cache_size"]))
            return True
