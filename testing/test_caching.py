#!/usr/bin/env python3
"""
Test module for  caching utility processing
"""

import logging
import os
import shutil
import unittest
import rring.tools.caching as caching

logger = logging.getLogger(__name__)


class TestDiskCache(unittest.TestCase):

    def setUp(self):
        "will setup the logger"
        pass

    def create_file(self, filename, content):
        with open(filename, "w") as _h_out:
            _h_out.write(content)

    def test_create_cache(self):
        "test the creation of a cache (and the errors)"
        logger = logging.getLogger('root' + '.test_create_cache')
        # test the nominal case: everything is OK
        pid = os.getgid()
        if os.path.exists(f"/tmp/{pid}_TEST_CACHE"):
            shutil.rmtree(f"/tmp/{pid}_TEST_CACHE")
        mycache = caching.DiskFileCache(f"/tmp/{pid}_TEST_CACHE", 2000, 'root')
        self.assertTrue(os.path.exists(f"/tmp/{pid}_TEST_CACHE"))
        self.assertTrue(os.path.isdir(f"/tmp/{pid}_TEST_CACHE"))
        self.assertTrue(os.path.exists(f"/tmp/{pid}_TEST_CACHE/.cache_config.json"))

        # open an existing cache
        mycache = caching.DiskFileCache(f"/tmp/{pid}_TEST_CACHE", logger='root')
        self.assertEqual(2000, mycache.get_limit())

        shutil.rmtree(f"/tmp/{pid}_TEST_CACHE")
        logger.info(f"removing /tmp/{pid}TEST_CACHE")

    def test_set(self):
        "test adding a file in the cache"
        logger = logging.getLogger('root.test_set')
        logger.info("test_set")
        files = [f"/tmp/{os.getpid()}_coucou", f"/tmp/{os.getpid()}_tata",
                 f"/tmp/{os.getpid()}_titi"]
        logger.debug("creating temps files {}".format(" ".join(files)))
#         for f in files:
#             self.create_file(f, "0123456789")
#         if os.path.exists(f"/tmp/{os.getpid()}_TEST_CACHE"):
#             shutil.rmtree(f"/tmp/{os.getpid()}_TEST_CACHE")
#         mycache = caching.DiskFileCache(f"/tmp/{os.getpid()}_TEST_CACHE", 21, 'root')
#         mycache.set(f"/tmp/{os.getpid()}_TEST_CACHE/coucou")
#         time.sleep(1)
#         self.assertTrue(os.path.exists(f"/tmp/{os.getpid()}_TEST_CACHE/coucou"))
#         self.assertEqual(10, mycache.get_size())
#         mycache.set(f"/tmp/{os.getpid()}_tata")
#         time.sleep(1)
#         self.assertTrue(os.path.exists(f"/tmp/{os.getpid()}_TEST_CACHE/tata"))
#         # not enough place: should remove coucou
#         mycache.set(f"/tmp/{os.getpid()}_titi")
#         self.assertFalse("coucou" in os.listdir(f"/tmp/{os.getpid()}_TEST_CACHE"))
#         self.assertEqual(20, mycache.get_size())
#         shutil.rmtree(f"/tmp/{os.getpid()}_TEST_CACHE")
#         logger.info("removing /tmp/TEST_CACHE")
#         for f in files:
#             os.unlink(f)
#         logger.info("removing temps files {}".format(" ".join(files)))

    def test_get(self):
        "test getting a file from cache"
        logger = logging.getLogger('root.test_set')
        logger.info("test_set")
#         if os.path.exists(f"/tmp/{os.getpid()}_TEST_CACHE"):
#             shutil.rmtree(f"/tmp/{os.getpid()}_TEST_CACHE")
#         mycache = caching.DiskFileCache(f"/tmp/{os.getpid()}_TEST_CACHE", 21, 'root')
#         logger.info("removing /tmp/TEST_CACHE")
#         self.create_file(f"/tmp/{os.getpid()}_toto", "0123456789")
#         self.assertFalse(mycache.get("fjkdlfqjkl", "/tmp"))
#         mycache.set(f"/tmp/{os.getpid()}_toto")
#         os.unlink(f"/tmp/{os.getpid()}_toto")
#         # getting toto in tmp dir
#         # self.assertTrue(mycache.get("toto", f"/tmp/{os.getpid()}_toto"))
#         self.assertIn("toto", os.listdir("/tmp"))
#         os.unlink(f"/tmp/{os.getpid()}_toto")
#         # getting toto in tmp dir as titi
#         self.assertTrue(mycache.get("toto", f"/tmp/{os.getpid()}_titi"))
#         self.assertNotIn("toto", os.listdir("/tmp"))
#         self.assertIn("titi", os.listdir("/tmp"))
#
#         # trying to get something with an absolute name: should raise an error
#         with self.assertRaises(ValueError):
#             mycache.get("/titi/toto", "/tmp")
#         os.unlink(f"/tmp/{os.getpid()}_titi")
#         shutil.rmtree(f"/tmp/{os.getpid()}_TEST_CACHE")


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestDiskCache)
    _res = unittest.TextTestRunner(verbosity=2).run(suite)
    import sys
    sys.exit(max(len(_res.failures), len(_res.errors)))
